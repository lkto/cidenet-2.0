# Prueba Tecnica CIDENET
## _Jesson Ember Bejarano Mosquera_

Dentro de sus procesos de Talento Humano, Cidenet S.A.S. requiere de un sistema que le permita registrar el ingreso y la salida de sus empleados, así como administrar su información. Actualmente el proceso se realiza de manera manual sobre una hoja de cálculo, lo cual funciona pero impide alimentar otros procesos para los cuales es importante esta información, así como llevar de manera óptima el registro y administración de la misma.

## Tecnologias

- [NodeJs] - HTML enhanced for web apps!
- [VueJs] - awesome web-based text editor
- [MySql] - Markdown parser done right. Fast and easy to extend.

## Instalacion
Requiere tener instalado en su computador [Node.js](https://nodejs.org/) v14.
Luego de tener instalado los requerimientos anteriores, clone el proyecto.
En este encontrara una carpeta llamada bd con un archivo db_cidenet_test.sql, ejecute ese script para crear la base de datos llamada db_cidenet_test

luego encontrar una carpeta llamada back entre en ella, abra la consola de comandos y ejecute los comandos:
```sh
npm install
npm start
```
esto iniciara el servidor node en el puerto 3000

luego devuelvase a la raiz, abra la carpeta front, abra la consola de comandos y ejecute los comandos:

```sh
npm install 
npm run serve
```

Esto desplegara el servidor front, el cual podra ser visto en el navegador en el puerto 8080.

Recuerde que si no ha levantado el servidor back, puede presentar inconsistencias el front

### Dudas

Cualquier duda o inconveniente a la hora de desplegar el proyecto, por favor comunicarmelo
